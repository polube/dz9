import { useState, useEffect } from "react";
import TodoItem from "./components/TodoItem";
import AddTodo from "./components/AddTodo";

function App() {
    const [todos, setTodos] = useState([]);

    const handleAddTodoItem = (theTitle) => {
        if (theTitle) {
            for (let i = 0; i < todos.length; i++) {
                if (todos[i].title === theTitle) {
                    return false;
                }
            }
            const theTodo = {
                id: todos.reduce((num, { id }) => num > id ? num : id, 0) + 1,
                completed: false,
                title: theTitle,
            }
            setTodos([theTodo, ...todos]);
        }

    }

    const handleChangeTodoItem = (id, completed) => {
        const todoIndex = todos.findIndex((item) => item.id === id);

        if (todoIndex !== -1) {
            const newTodos = [...todos];
            const todo = newTodos[todoIndex];

            newTodos.splice(todoIndex, 1, { ...todo, completed });

            setTodos(newTodos);
        }
    };

    const handlerDeleteTodoItem = (id) => {
        const newTodos = todos.filter((todo) => todo.id !== id);

        setTodos(newTodos);
    }

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/todos?userId=1")
            .then((response) => response.json())
            .then((data) => {
                setTodos(data)
            });
    }, []);

    return (
        <div className="container">
            <h2>{"TODO List:"}</h2>
            <div style={{ margin: "24px 0 48px" }}>
                <AddTodo onAdd={handleAddTodoItem} />
            </div>
            <ul className="collection">
                {todos.map((todo) => <TodoItem
                    key={todo.id}
                    {...todo}
                    onChange={handleChangeTodoItem}
                    onDelete={handlerDeleteTodoItem}
                />)}
            </ul>
        </div>
    );
}

export default App;
