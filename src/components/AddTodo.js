
const AddTodo = ({ onAdd }) => {
    return (<>
        <input id="add-todo" onKeyPress={(event) => {
            if (event.charCode === 13) {
                onAdd(document.getElementById('add-todo').value);
            }
        }} />
        <label htmlFor="add-todo">{"Add New Todo"}</label>
    </>);
}

export default AddTodo;
